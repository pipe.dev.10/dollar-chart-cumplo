import React from "react";
import { Title, BannerHeader } from "./style";

export const Header = () => {
  return (
    <BannerHeader>
      <header>
        <Title>Dollar Cumplo</Title>
      </header>
    </BannerHeader>
  );
};
