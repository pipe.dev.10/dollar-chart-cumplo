import React, { useState } from "react";
import ReactApexChart from "react-apexcharts";
import { useChartHook } from "./chartDollarHook";

export const ChartDollar = (props) => {
  const { data, isError } = props;
  const { getMin, getMax, getAverage, getConfigChart } = useChartHook(data);

  if (isError) {
    return <p>Hubo un error favor intentar mas tarde</p>;
  }

  return (
    <div
      style={{
        width: "400px",
        height: "300px",
      }}
    >
      <h1>Data Chart</h1>
      {data.length && (
        <div>
          <p className="max-value">Min: {getMin()}</p>
          <p className="min-value">Max: {getMax()}</p>
          <p className="average-value">Average: {getAverage()}</p>
          <ReactApexChart
            options={getConfigChart().options}
            series={getConfigChart().series}
            type="line"
            height={350}
          />
        </div>
      )}
    </div>
  );
};
