import { useState } from "react";

const useChartHook = (data) => {
  const getMin = () => {
    return data.length
      ? data.reduce(
          (min, dollar) => (dollar.Valor < min ? dollar.Valor : min),
          "9999999"
        )
      : 0;
  };

  const getMax = () => {
    return data.length
      ? data.reduce(
          (max, dollar) => (dollar.Valor > max ? dollar.Valor : max),
          "0"
        )
      : 0;
  };

  const getAverage = () => {
    const average =
      data.length &&
      data.reduce(
        (total, dollar) => total + parseFloat(dollar.Valor.replace(",", ".")),
        0
      ) / data.length;
    return average.toFixed(2).replace(".", ",");
  };

  const getConfigChart = () => {
    const fechaList = [];
    const dollarList = [];
    data.map((dollar) => {
      fechaList.push(dollar.Fecha);
      dollarList.push(dollar.Valor);
    });
    const config = {
      options: {
        chart: {
          id: "date",
        },
        xaxis: {
          categories: fechaList,
        },
      },
      series: [
        {
          name: "dollar",
          data: dollarList,
        },
      ],
    };
    return config;
  };

  return {
    getMin,
    getMax,
    getAverage,
    getConfigChart,
  };
};

export { useChartHook };
