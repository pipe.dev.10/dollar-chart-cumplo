import React, { useState, useEffect, useCallback } from "react";
import { Container, Label, Button } from "./style";
import apiConfig from "../../config/apiConfig";
import { ChartDollar } from "../ChartDollar";

export const FormDollar = () => {
  const today = new Date();
  const month =
    String(today.getMonth() + 1).length === 1
      ? `0${today.getMonth() + 1}`
      : today.getMonth();
  const dateComplete = `${today.getFullYear()}-${month}-${today.getDate()}`;

  const [dataDollar, setDataDollar] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [startDate, setStartDate] = useState(`${dateComplete}`);
  const [endDate, setEndDate] = useState(`${dateComplete}`);

  const getDollars = async () => {
    try {
      const apiDollar = apiConfig.getDollarByDates(startDate, endDate);

      setIsLoading(true);
      let response = await fetch(apiDollar);
      response = await response.json();
      setDataDollar(response.Dolares);
      setIsLoading(false);
    } catch (error) {
      console.log(`Error: ${error}`);
      setIsError(true);
      setIsLoading(false);
    }
  };

  useEffect(() => {}, [dataDollar]);

  return (
    <Container>
      <div>
        <Label htmlFor="from">From</Label>
        <input
          type="date"
          id="start"
          name="trip-start"
          value={startDate}
          onChange={(e) => setStartDate(e.target.value)}
        />
        <Label htmlFor="to">To</Label>
        <input
          type="date"
          id="end"
          name="trip-end"
          value={endDate}
          onChange={(e) => setEndDate(e.target.value)}
        />
        <Button id="contact-submit" onClick={getDollars}>
          Get Dollar
        </Button>
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <ChartDollar data={dataDollar} isError={isError} />
        )}
      </div>
    </Container>
  );
};
