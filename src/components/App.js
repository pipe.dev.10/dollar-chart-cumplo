import React from "react";
import { Header } from "./Header";
import { FormDollar } from "./FormDollar";

const App = () => (
  <div>
    <Header />
    <FormDollar />
  </div>
);

export default App;
