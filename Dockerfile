# Official build of Nginx
FROM nginx:1.17.9-alpine

# Setup custom nginx configuration
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# Setup authentication file
# COPY auth /etc

# Custom content
COPY dist /usr/share/nginx/html
